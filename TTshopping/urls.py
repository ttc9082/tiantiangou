from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^$', include('tiantiangou.urls', namespace='ttg')),
    url(r'^$', 'tiantiangou.views.products', name='products'),
    url(r'^products/$', 'tiantiangou.views.products', name='products'),
    url(r'^orders/$', 'tiantiangou.views.orders', name='orders'),
    url(r'^about/$', 'tiantiangou.views.about', name='about'),
    url(r'^manage/$', 'tiantiangou.views.manage', name='manage'),
    url(r'^add/$', 'tiantiangou.views.add', name='add'),
    url(r'^uploadpicsfor/(?P<product_id>\d+)/$', 'tiantiangou.views.upload', name='upload'),
    url(r'^brandandtype/(?P<product_id>\d+)/$', 'tiantiangou.views.brand_type', name='brand_type'),
    url(r'^create_brand/(?P<product_id>\d+)/$', 'tiantiangou.views.create_brand', name='create_brand'),
    url(r'^create_type/(?P<product_id>\d+)/$', 'tiantiangou.views.create_type', name='create_type'),
    url(r'^edit/(?P<product_id>\d+)/$', 'tiantiangou.views.add', name='edit'),
    url(r'^logout/$', 'tiantiangou.views.logout', name='logout'),
    url(r'^productadded/(?P<product_id>\d+)/$', 'tiantiangou.views.product_success', name='productadded'),
    url(r'^del/(?P<pic_id>\d+)/$', 'tiantiangou.views.delete_pic', name='delete'),
)
