from django.shortcuts import render_to_response, render, redirect
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.views.decorators.csrf import csrf_protect
from django.core.context_processors import csrf
from .models import *
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from utils import aws
from PIL import Image
import urllib2
from forms import *
from django.core.urlresolvers import reverse
# Create your views here.

def products(request):
    user = request.user
    products = Product.objects.all().order_by('-create_time')
    brands = Brand.objects.all()
    types = Type.objects.all()
    context = {'page': 'products',
               'products': products,
               'brands': brands,
               'types': types,
               'user': user}
    return render_to_response('products.html', context)

def orders(request):
    context = {'page': 'orders',
               'user': request.user}
    return render_to_response('orders.html', context)

def about(request):
    context = {'page': 'about',
               'user': request.user}
    return render_to_response('about.html', context)

def manage(request):
    context = {}
    context.update(csrf(request))
    if request.method == 'POST':
        password = request.POST.get('password', '')
        user = auth.authenticate(username='ttc9082', password=password)
        if user is not None:
            auth.login(request, user)
            return HttpResponseRedirect(reverse('products'))
        context = {'error': 'Wrong Pass, Try again.'}
        return render(request, 'manage.html', context)
    return render(request, 'manage.html', context)

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')

def add(request, product_id=None):
    context = {}
    if product_id:
        context['edit'] = 1
        context['product_id'] = product_id
        form = ProductForm(instance=Product.objects.get(pk=product_id))
        if request.method == 'POST':
            form = ProductForm(request.POST, instance=Product.objects.get(pk=product_id))
            if form.is_valid():
                p = form.save()
                return HttpResponseRedirect(reverse('brand_type', args=[p.pk]))
    else:
        if request.method == 'POST':
            form = ProductForm(request.POST)
            if form.is_valid():
                p = form.save()
                return HttpResponseRedirect(reverse('brand_type', args=[p.pk]))
        else:
            form = ProductForm()
    context['ProductForm'] = form
    context.update(csrf(request))
    return render(request, 'addproduct.html', context)

def upload(request, product_id):
    pro = Product.objects.get(pk=product_id)
    pics = pro.pic_set.all()
    context = {'pk': product_id,
               'pics': pics}
    context.update(csrf(request))
    if request.method == 'POST':
        picture = request.FILES['file']
        if picture.name[-4:] in ['.jpg', '.png', '.gif', '.PNG', '.JPG', '.GIF', 'jpeg', 'JPEG']:
            url = aws.save_to_s3(picture)
            pic_file = urllib2.urlopen(url)
            localFile = open('temp.png', 'w')
            localFile.write(pic_file.read())
            localFile.close()
            localFile = open('temp.png', 'r')
            s = Image.open(localFile)
            width, height = s.size
            final_height = height * 190 / width
            new_pic = Pic(url=url, create_time=timezone.now(), final_height=final_height,
                          product=Product.objects.get(pk=product_id))
            new_pic.save()
        return HttpResponseRedirect(reverse('productadded', args=[product_id]))
    return render(request, 'upload.html', context)

def brand_type(request, product_id):
    if request.method == 'POST':
        brand = request.POST.get('brand_id')
        type = request.POST.get('type_id')
        p = Product.objects.get(pk=product_id)
        p.brand = Brand.objects.get(pk=brand)
        p.type = Type.objects.get(pk=type)
        p.create_time = timezone.now()
        p.save()
        return HttpResponseRedirect(reverse('upload', args=[product_id]))
    BForm = BrandForm()
    TForm = TypeForm()
    context = {'BrandForm': BForm,
               'TypeForm': TForm,
               'pk': product_id,
               'product': Product.objects.get(pk=product_id),
               'brands': Brand.objects.all(),
               'types': Type.objects.all()}
    context.update(csrf(request))
    return render(request, 'brandandtype.html', context)

def create_brand(request, product_id):
    if request.method == 'POST':
        BForm = BrandForm(request.POST)
        if BForm.is_valid():
            BForm.save()
    return HttpResponseRedirect(reverse('brand_type', args=[product_id]))

def create_type(request, product_id):
    if request.method == 'POST':
        TForm = TypeForm(request.POST)
        if TForm.is_valid():
            TForm.save()
    return HttpResponseRedirect(reverse('brand_type', args=[product_id]))

def product_success(request, product_id=0):
    context = {'product_id': product_id}
    return render_to_response('success.html', context)

def delete_pic(request, pic_id):
    picture = Pic.objects.get(pk=pic_id)
    picture.delete()
    product_id = picture.product.id
    return HttpResponseRedirect(reverse('upload', args=[product_id]))
