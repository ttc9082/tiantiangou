from django import forms
from django.forms import ModelForm
from models import *

class ProductForm(ModelForm):
    class Meta:
        model = Product
        # fields = '__all__'
        exclude = ['create_time', 'pic', 'brand', 'type']

class BrandForm(ModelForm):
    class Meta:
        model = Brand
        fields = '__all__'

class TypeForm(ModelForm):
    class Meta:
        model = Type
        fields = '__all__'