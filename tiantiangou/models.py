from django.db import models
from django.contrib.auth.models import User

class Brand(models.Model):
    name = models.CharField(max_length=50, unique=True)
    def __unicode__(self):
        return name

class Type(models.Model):
    name = models.CharField(max_length=50, unique=True)
    def __unicode__(self):
        return name

class Product(models.Model):
    name = models.CharField(max_length=50)
    price = models.IntegerField()
    link = models.TextField()
    des = models.TextField()
    brand = models.ForeignKey(Brand, null=True)
    type = models.ForeignKey(Type, null=True)
    create_time = models.DateTimeField(null=True)
    def __unicode__(self):
        return self.name

class Pic(models.Model):
    product = models.ForeignKey(Product)
    url = models.TextField()
    create_time = models.DateTimeField()
    final_height = models.IntegerField()
    def __unicode__(self):
        return self.url

class Order(models.Model):
    product = models.ForeignKey(Product)
    buyer = models.CharField(max_length=50)
    number = models.IntegerField()
    purchase_date = models.DateTimeField()
    total_price = models.IntegerField()
    def __unicode__(self):
        return Product

class Process(models.Model):
    order = models.ForeignKey(Order)
    pic = models.ManyToManyField(Pic)
    date = models.DateTimeField()
    comment = models.TextField()
    def __unicode__(self):
        return Order