from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'tiantiangou.views.products', name='products'),
    url(r'^products/$', 'tiantiangou.views.products', name='products'),
    url(r'^orders/$', 'tiantiangou.views.orders', name='orders'),
    url(r'^about/$', 'tiantiangou.views.about', name='about'),
    url(r'^manage/$', 'tiantiangou.views.manage', name='manage'),
    url(r'^add/$', 'tiantiangou.views.add', name='add'),
    url(r'^uploadpicsfor/(?P<product_id>\d+)/$', 'tiantiangou.views.upload', name='upload'),
    url(r'^brandandtype/(?P<product_id>\d+)/$', 'tiantiangou.views.brand_type', name='brand_type'),
    url(r'^create_brand/(?P<product_id>\d+)/$', 'tiantiangou.views.create_brand', name='create_brand'),
    url(r'^create_type/(?P<product_id>\d+)/$', 'tiantiangou.views.create_type', name='create_type'),
    # url(r'^repin_pin/(?P<pin_id>\d+)/$', 'pin.views.repin_pin', name='repin'),
    # url(r'^change_profile/$', 'pin.views.change_profile', name='change_profile'),
)
