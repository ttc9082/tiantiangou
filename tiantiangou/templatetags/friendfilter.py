from django import template
from tiantiangou.models import *
# Create your views here.
# This is a filter, when loaded, using functions below in templates are enabled.

register = template.Library()

@register.filter
def p_pic(product_id):
    p = Product.objects.get(pk=product_id)
    ps = p.pic_set.all()[0]
    return ps

@register.filter
def friend_by(friend, user_id):
    return friend.filter(frding_user=user_id)